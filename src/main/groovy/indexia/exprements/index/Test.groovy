package indexia.exprements.index

import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanIndexQuery
import com.thinkaurelius.titan.core.attribute.Text
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.Vertex
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.commons.configuration.DefaultConfigurationBuilder

/**
 * Created by erezagami on 8/7/14.
 */
class Test {
    public static void main2(String[] args) {
        Graph g = createGraph();

        g.makeKey("name")
                .dataType(String.class)
                .indexed("search", Vertex.class)
                .make();

        g.makeKey("type")
                .dataType(String.class)
                .indexed("search", Vertex.class)
                .make();

        Vertex res1 = g.addVertex(12);
        res1.setProperty("name", "res1")
        res1.setProperty("type", "resource")

        Vertex res2 = g.addVertex(null);
        res2.setProperty("name", "res2")
        res2.setProperty("type", "resource")

        Vertex v1 = g.addVertex(null);
        v1.setProperty("name", "erez");
        v1.setProperty("type", "person");
        g.addEdge(null, v1, res1, "of_resource")

        Vertex v2 = g.addVertex(null);
        v2.setProperty("name", "yoda");
        v2.setProperty("type", "movie-start");

        Vertex v3 = g.addVertex(null);
        v3.setProperty("name", "banana")
        v3.setProperty("type", "fruit");

        Vertex v4 = g.addVertex(null);
        v4.setProperty("name", "erez")
        v4.setProperty("type", "movie");
        g.addEdge(null, v4, res2, "of_resource")

        g.query().has("asdf","Asdf").edges().hasProperty()


        g.makeKey("new").dataType(Integer.class).make();

        Vertex v5 = g.addVertex(null);
        v5.setProperty("name", "a full story for you all")
        v5.setProperty("type", "person");
        v5.setProperty("new",44)
        g.addEdge(null, v5, res2, "of_resource")

        Vertex v6 = g.addVertex(null);
        v6.setProperty("name", 22);
        v6.setProperty("type", 22);
        //cause exception coz key is already int
        //v6.setProperty("new","blabla")

        g.commit();




        List<Vertex> l = g.query().has("name", Text.CONTAINS, "erez")
                        .has("type", "person").vertices().asList();

        List<TitanIndexQuery.Result<Vertex>>  t = g.indexQuery("search","v.name:(ab fully storo) AND v.type: 'person'").vertices().asList()


        //t.get(0).element.getProperty("type")


        g.shutdown();

    }

    public static void main1(String[] args) {
        Configuration c = new BaseConfiguration();
        c.addProperty("storage.directory","/tmp/testgraph");
        Graph g = TitanFactory.open(c);

        Vertex v1 = g.addVertex(null);
        Vertex v2 = g.addVertex(null);
        Edge e = g.addEdge(null, v1, v2, "LINK");
        g.commit();
        e.setProperty("key", "foo");
        g.commit();
        println "!!!!" + e.getProperty("ID");
    }

    public static void main(String[] removeVertex) {
        Graph g = createGraph();
        Vertex v1 = g.addVertex(null);
        Vertex v2 = g.addVertex(null);
        Edge e = g.addEdge(null, v1, v2, "EDGE!");
        g.commit();
        e = g.getEdge(e.getId());
        g.removeEdge(e);
        g.commit();
    }

    public static Graph createGraph() {
        File f = new File("/tmp/testgraph")
        if (f.exists()) {
            if (f.isDirectory()) {
                f.deleteDir()
            } else {
                f.delete()
            }
        }

        Configuration c = new BaseConfiguration();
        //c.addProperty("storage.index.search.backend","lucene")
        //c.addProperty("storage.index.search.directory","/tmp/testgraph/searchindex");
        //c.addProperty("storage.directory","/tmp/testgraph");
        c.addProperty("storage.backend","inmemory")

        Graph g = TitanFactory.open(c);
        //multiple titan instances seem to work
        //Graph g1 = TitanFactory.open(c);
        //Graph g = new Neo4j2Graph("/tmp/testgraph");

        return g;
    }
}
