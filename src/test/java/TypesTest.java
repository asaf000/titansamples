import com.thinkaurelius.titan.core.Cardinality;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import org.junit.Test;

/**
 * Tests for Data Types
 */
public class TypesTest extends Base {
    @Test
    public void testSamePropertyForEdgeAndVertex() {
        //We can create the same key for both vertex and edge
        TitanManagement management = tg.getManagementSystem();
        management.makePropertyKey("TYPE").dataType(String.class).cardinality(Cardinality.SINGLE).make();
    }
}
