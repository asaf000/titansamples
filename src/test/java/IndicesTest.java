import com.google.common.collect.Lists;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.VertexLabel;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Vertex;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 */
public class IndicesTest extends Base {
    @Test
    public void uniquenessPerVertexLabelTest() {
        TitanManagement mgmt = tg.getManagementSystem();
        mgmt.makeVertexLabel("account").make();
        mgmt.makeVertexLabel("permission").make();
        PropertyKey name = mgmt.makePropertyKey("name").dataType(String.class).make();
        PropertyKey age = mgmt.makePropertyKey("age").dataType(String.class).make();
        VertexLabel account = mgmt.getVertexLabel("account");
        mgmt.buildIndex("accountByName", Vertex.class).addKey(mgmt.getPropertyKey("name")).indexOnly(mgmt.getVertexLabel("account")).unique().buildCompositeIndex();
        mgmt.commit();

        //Should not fail
        TitanVertex acc1 = tg.addVertexWithLabel("account");
        acc1.setProperty("name", "acc1");
        acc1.setProperty("age", 9);
        TitanVertex perm = tg.addVertexWithLabel("permission");
        perm.setProperty("name", "acc1");


        //Should fail
        try {
            TitanVertex acc11 = tg.addVertexWithLabel("account");
            acc11.setProperty("name", "acc1");
            assertThat(true, is(false));
        } catch (IllegalArgumentException e) {
            assertThat(true, is(true));
        }

        //Full scan because we must search for label too since composite index include it
        System.out.println("FULL SCAN: " + Lists.newArrayList(tg.query().has("name", "moo").vertices()).size());

        //Doesn't cause full scan, I guess because key names are case sensitive, since key doesn't exist there's no reason to search at all
        System.out.println(Lists.newArrayList(tg.query().has("NAME", "cow").vertices()).size());

        //Doesn't cause full scan, since key 'foo' does not exist
        System.out.println(Lists.newArrayList(tg.query().has("foo", "bar").vertices()).size());

        //Hit the index since all keys in the composite index are there
        System.out.println(Lists.newArrayList(tg.query().has("name", "acc1").has("label", "account").vertices()).size());

        //No full scan because foo does not exist so there's no chance that query will return anything
        System.out.println(Lists.newArrayList(tg.query().has("name", "moo1").has("foo", "bar").vertices()).size());

        //Full scan, we have no label
        System.out.println("FULL SCAN:" + Lists.newArrayList(tg.query().has("name", "acc1").has("age", 9).vertices()).size());

        //Extra keys should be ok
        System.out.println(Lists.newArrayList(tg.query().has("name", "acc1").has("label", "account").has("age", 9).vertices()).size());
    }

    @Test
    public void uniquenessPerVertexLabelForMultiProp() {
        TitanManagement mgmt = tg.getManagementSystem();
        PropertyKey name = mgmt.makePropertyKey("name").dataType(String.class).make();
        PropertyKey resource = mgmt.makePropertyKey("resource").dataType(String.class).make();
        VertexLabel account = mgmt.getVertexLabel("account");
        mgmt.buildIndex("accountByName", Vertex.class).addKey(name).addKey(resource).indexOnly(account).unique().buildCompositeIndex();
        mgmt.commit();

        TitanVertex acc1OnRes1 = tg.addVertexWithLabel("account");
        acc1OnRes1.setProperty("name", "acc1");
        acc1OnRes1.setProperty("resource", "AD");
        TitanVertex acc1OnRes2 = tg.addVertexWithLabel("account");
        acc1OnRes2.setProperty("name", "acc1");
        acc1OnRes2.setProperty("resource", "CRM");
        tg.commit();

        TitanVertex perm = tg.addVertexWithLabel("perm");
        perm.setProperty("name", "acc1");
        perm.setProperty("resource", "AD");

        TitanVertex acc1OnRes1Again = tg.addVertexWithLabel("account");
        acc1OnRes1Again.setProperty("name", "acc1");
        acc1OnRes1Again.setProperty("resource", "AD");
    }
}
