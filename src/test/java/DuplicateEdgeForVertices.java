import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import org.junit.Test;

public class DuplicateEdgeForVertices extends Base {
    @Test
    public void test() {
        Vertex v1 = tg.addVertex(null);
        Vertex v2 = tg.addVertex(null);
        Edge e = tg.addEdge(null, v1, v2, "zzzz");
        tg.addEdge(null, v1, v2, "zzzz");
    }
}
