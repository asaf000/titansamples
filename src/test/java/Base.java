import com.thinkaurelius.titan.core.TitanFactory;
import com.thinkaurelius.titan.core.TitanGraph;
import org.junit.Before;

public class Base {
    protected TitanGraph tg;

    @Before
    public void initGraph() {
        if (tg != null) {
            tg.shutdown();
        }
        tg = TitanFactory.build().set("storage.backend", "inmemory").open();//.set("schema.default", "none").open();
    }
}
