import com.google.common.collect.Lists;
import com.thinkaurelius.titan.core.PropertyKey;
import com.thinkaurelius.titan.core.TitanVertex;
import com.thinkaurelius.titan.core.VertexLabel;
import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Vertex;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNull.notNullValue;

public class VertexLabels extends Base {
    @Test
    public void simple() {
        //Create a label
        TitanManagement mgmt = tg.getManagementSystem();
        VertexLabel accountLabel = mgmt.makeVertexLabel("account").make();
        //We can save some info on the label level
        accountLabel.setProperty("description", "An account is an identifier of a human or service on a resource");
        PropertyKey label = mgmt.getPropertyKey("label");
        mgmt.buildIndex("asdflkasdf", Vertex.class).addKey(label).buildCompositeIndex();
        mgmt.commit();

        //Can add vertices for that label
        TitanVertex v1 = tg.addVertexWithLabel("account");
        v1.setProperty("name", "asdf");
        tg.addVertexWithLabel("account");
        tg.addVertexWithLabel("account");
        tg.addVertexWithLabel("perm");
        assertThat(v1.getLabel(), is("account"));
        VertexLabel accLabel = tg.getVertexLabel("account");
        tg.commit();
        //assertThat(Lists.newArrayList(tg.getManagementSystem().getVertexLabels()).size(), is(1));
        assertThat(tg.getManagementSystem().getVertexLabels(), hasItem(accLabel));

        //Can get the label from a vertex
        assertThat(v1.getLabel(), is("account"));
        assertThat(v1.getVertexLabel().getProperty("description"), notNullValue());

        tg.query().vertices();
        System.out.println(tg.query().has("label", "account").vertices().iterator().hasNext());


        //accountLabel = tg.getVertexLabel("account");

        //TODO:
        //VertexLabel is actually a vertex that can have edges, what that means? when can a label vertex have edges ?
    }
}
