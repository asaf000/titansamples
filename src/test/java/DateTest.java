import com.thinkaurelius.titan.core.schema.TitanManagement;
import com.tinkerpop.blueprints.Vertex;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DateTest extends Base {
    @Test
    public void test() {
        TitanManagement tm = tg.getManagementSystem();
        tm.makePropertyKey("time").dataType(Date.class).make();
        tm.commit();
        Vertex v1 = tg.addVertex(null);
        Date d = new Date();
        v1.setProperty("time", d);
        System.out.println(d.getTime());

        v1 = tg.getVertex(v1.getId());
        Date ld1 = v1.getProperty("time");
        System.out.println(ld1.getTime());

        assertThat(ld1, is(d));
    }
}
